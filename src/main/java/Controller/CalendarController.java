package Controller;

import Model.AdminModel;
import Model.FamilyModel;
import View.CalendarView.CalendarView;
import View.LoginView;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Array;
import java.util.Arrays;
import java.util.Scanner;

public class CalendarController {

    private CalendarView view;

    public CalendarController(CalendarView view) {
        this.view = view;

        try{
            getAllFamilys();

        } catch (Exception e) {

            e.getMessage();
        }
    }

    public String getAllFamilys() throws ProtocolException {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/familys");

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();

            int responseCode = con.getResponseCode();

            if (responseCode != 200) {
                throw new RuntimeException("HttpResponseCode" + responseCode);
            } else {
                StringBuilder informationString = new StringBuilder();
                Scanner scanner = new Scanner(url.openStream());

                while (scanner.hasNext()) {
                    informationString.append(scanner.nextLine());
                }
                scanner.close();
                System.out.println(informationString);
                Gson g = new Gson();


                FamilyModel[] familys = g.fromJson(String.valueOf(informationString), FamilyModel[].class);


                String[] familyNames = new String[familys.length];

                for (int i = 0; i < familys.length; i++) {

                    familyNames[i] = familys[i].getFamilyNaam();
                }

                view.configureView(familyNames);

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
