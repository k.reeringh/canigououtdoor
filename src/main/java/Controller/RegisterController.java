package Controller;

import Model.AdminModel;
import Model.FamilyModel;
import View.LoginView;
import View.RegisterView;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class RegisterController {

    //Controleert of er geen null-waarden tussen de user-input zit, maakt family-object aan en slaat deze op in DB

    private RegisterView view;
    private FamilyModel model;


    public RegisterController() {

    }


//    public String getAllFamilys() throws ProtocolException {
//        URL url = null;
//        try {
//            url = new URL("http://localhost:8080/familys");
//
//            HttpURLConnection con = (HttpURLConnection) url.openConnection();
//            con.setRequestMethod("GET");
//            con.connect();
//
//            int responseCode = con.getResponseCode();
//
//            if (responseCode != 200) {
//                throw new RuntimeException("HttpResponseCode" + responseCode);
//            } else {
//                StringBuilder informationString = new StringBuilder();
//                Scanner scanner = new Scanner(url.openStream());
//
//                while (scanner.hasNext()) {
//                    informationString.append(scanner.nextLine());
//                }
//                scanner.close();
//                System.out.println(informationString);
//                Gson g = new Gson();
//
//                try {
//                   FamilyModel []  s = g.fromJson(String.valueOf(informationString), FamilyModel[].class);
//                    System.out.println(s[0].getFamilyNaam());
//                }   catch (JsonParseException e){
//                    System.out.println( e.getMessage());
//                }
//
//                // int id= family.getId();
//              //  System.out.println(family.getFamilyNaam());
//
//                //    JSONObject object= new JSONObject(informationString);
//                //String familyName= object.getJSONObject("familyName").toString();
//                //System.out.println(family);
//                // FamilyModel obj = JSON.parse(text);
//
//                // return informationString.toString();
//
//            }
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public RegisterView getView() {
        return view;
    }

    public void setView(RegisterView view) {
        this.view = view;
    }

    public boolean registerFamily(int id, String familyNaam, int campingId, String telefoonummer) {

        URL url = null;
        try {
            url = new URL("http://localhost:8080/register");

            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Accept", "application/json");
            http.setRequestProperty("Content-Type", "application/json");

            String data = "{\"familys\":[{\"family_id\":1,\"familyName\":\"janssen\",\"camping_id\":1,\"telefoonummer\":\"0000000\"}]}";

            byte[] out = data.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = http.getOutputStream();
            stream.write(out);

            int responseCode = http.getResponseCode();
            http.disconnect();

            if (responseCode == HttpStatus.SC_NOT_FOUND) {
                System.out.println("No user is found. Please enter valid credentials");
                return false;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }return true;

    }

}

