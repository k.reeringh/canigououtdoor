package Controller;
import Model.AdminModel;
import View.LoginView;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class LoginController {

    //Controleert of ingevulde waarden overeenkomen met gegevens in de AdminModel indien dit klopt MainView weergeven

    private LoginView view;
    private AdminModel model;

    public LoginController(LoginView view){
        this.view = view;
        model = new AdminModel();
    }
    //connects with backend to checks credentials in database
    public boolean checkCredentials(String username, String password) {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/login");

            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Accept", "application/json");
            http.setRequestProperty("Content-Type", "application/json");

            String data = "{ \"username\": \""+username+"\", \"password\":\""+password+"\"}";

            byte[] out = data.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = http.getOutputStream();
            stream.write(out);

            int responseCode = http.getResponseCode();
            http.disconnect();

            if(responseCode == HttpStatus.SC_NOT_FOUND)
            {
                System.out.println("No user is found. Please enter valid credentials");  //todo : show error message to user
                return false;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}

