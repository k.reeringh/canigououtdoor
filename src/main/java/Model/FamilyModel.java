package Model;

public class FamilyModel
{
    private int family_id;
    private String familyName;
    private int camping_id;
    private String telefoonummer;
    private int age;
    private Double weigth;
    private Double heigth;
    private Double shoeSize;


    public FamilyModel(int id, String familyName, int campingId,
                       String telefoonummer, int age, Double weigth,
                       Double heigth, Double shoeSize) {
        this.family_id = id;
        this.familyName = familyName;
        this.camping_id = campingId;
        this.telefoonummer = telefoonummer;
        this.age = age;
        this.weigth = weigth;
        this.heigth = heigth;
        this.shoeSize = shoeSize;
    }

    public int getId() {
        return family_id;
    }

    public void setId(int id) {
        this.family_id = id;
    }

    public String getFamilyNaam() {
        return familyName;
    }

    public void setFamilyNaam(String familyNaam) {
        this.familyName = familyNaam;
    }

    public int getCampingId() {
        return camping_id;
    }

    public void setCampingId(int campingId) {
        this.camping_id = campingId;
    }

    public String getTelefoonummer() {
        return telefoonummer;
    }

    public void setTelefoonummer(String telefoonummer) {
        this.telefoonummer = telefoonummer;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Double getWeigth() {
        return weigth;
    }

    public void setWeigth(Double weigth) {
        this.weigth = weigth;
    }

    public Double getHeigth() {
        return heigth;
    }

    public void setHeigth(Double heigth) {
        this.heigth = heigth;
    }

    public Double getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(Double shoeSize) {
        this.shoeSize = shoeSize;
    }

    @Override
    public String toString() {
        return "FamilyModel{" +
                "id=" + family_id +
                ", familyNaam='" + familyName + '\'' +
                ", campingId=" + camping_id +
                ", telefoonummer='" + telefoonummer + '\'' +
                ", age=" + age +
                ", weigth=" + weigth +
                ", heigth=" + heigth +
                ", shoeSize=" + shoeSize +
                '}';
    }
}
