
import Controller.RegisterController;
import View.Frame;
import javax.swing.*;
import java.io.IOException;
import java.rmi.registry.Registry;

public class Main {

    //Maakt LoginView aan en geeft deze weer

    public static void main(String[] args) throws IOException {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    createAndShowGUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void createAndShowGUI() throws Exception {
        new Frame();
    }

}
