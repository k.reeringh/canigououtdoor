package View.CalendarView;

import org.threeten.bp.DayOfWeek;
import org.threeten.extra.YearWeek;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.time.LocalDate;
import java.util.GregorianCalendar;

public class MonthCalendarView extends CalendarView
{

    public MonthCalendarView()
    {
        //Settings MonthCalendarView JPanel
        this.setLayout(new BorderLayout(5, 5));
        this.addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                super.componentResized(e);
                table.setRowHeight((e.getComponent().getHeight() - 49) / 6 );
            }
        });
        this.add(containerControls, BorderLayout.PAGE_START);
        this.add(scrollPane, BorderLayout.CENTER);

        comboBoxMonth = new JComboBox();
        //populate comboBoxMonth
        for (int i = 0; i < months.length; i++){
            comboBoxMonth.addItem(months[i]);
        }
        containerControls.add(comboBoxMonth);

        //Set row/column count
        mtblCalendar.setRowCount(6);

        preButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (calc.currentMonth == 0) {
                    calc.currentMonth = 11;
                    calc.currentYear -= 1;
                } else {
                    calc.currentMonth -= 1;
                }
                refreshCalendar(calc.currentMonth, calc.currentYear);
            }
        });

        nextButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (calc.currentMonth == 11) {
                    calc.currentMonth = 0;
                    calc.currentYear += 1;
                } else {
                    calc.currentMonth += 1;
                }
                refreshCalendar(calc.currentMonth, calc.currentYear);
            }
        });

        comboBoxYear.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (comboBoxYear.getSelectedItem() != null) {
                    String b = comboBoxYear.getSelectedItem().toString();
                    calc.currentYear = Integer.parseInt(b);
                    refreshCalendar(calc.currentMonth, calc.currentYear);
                }
            }
        });

        comboBoxMonth.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (comboBoxMonth.getSelectedItem() != null) {
                    calc.currentMonth = comboBoxMonth.getSelectedIndex();
                    refreshCalendar(calc.currentMonth, calc.currentYear);
                }
            }
        });

        //Refresh calendar
        refreshCalendar(calc.getRealMonth(), calc.getRealYear());
    }

    @Override
    protected void addHeaders() {
        //Add headers
        String[] headers = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        for(int i = 0; i< 7;i++) {
            mtblCalendar.addColumn(headers[i]);
        }
    }

    private void refreshCalendar(int month, int year)
    {
        newMonth = month;
        newYear = year;

        labelCurrentMonth.setText(months[month]);
        comboBoxYear.setSelectedItem(String.valueOf(year));
        comboBoxMonth.setSelectedItem(months[month]);

        preButton.setEnabled(true);
        nextButton.setEnabled(true);

        if (month == 0 && year <= calc.getRealYear() - 10)
        {
            preButton.setEnabled(false);
        } //Too early

        if (month == 11 && year >= calc.getRealYear() + 100)
        {
            nextButton.setEnabled(false);
        } //Too late

        //Clear table
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                mtblCalendar.setValueAt(null, i, j);

            }
        }

        //Get first day of month and number of days
        int nod, som;
        GregorianCalendar cal = new GregorianCalendar(year, month, 1);
        nod = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        som = cal.get(GregorianCalendar.DAY_OF_WEEK);

        //Draw calendar
        for (int i = 1; i <= nod; i++)
        {
            int row = (i + som - 2) / 7;
            int column = (i + som - 2) % 7;
            mtblCalendar.setValueAt(i, row, column);
        }

        CustomTableCellRenderer cellRenderer = new CustomTableCellRenderer();
        table.setDefaultRenderer(table.getColumnClass(0), cellRenderer);
    }
}
