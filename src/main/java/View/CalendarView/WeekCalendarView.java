package View.CalendarView;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.time.DayOfWeek;
import java.time.LocalDate;

import org.joda.time.DateTime;
import org.threeten.extra.YearWeek;

import javax.swing.*;

public class WeekCalendarView extends CalendarView
{
    public WeekCalendarView()
    {
        //Settings WeekCalendarView JPanel
        this.setLayout(new BorderLayout(5, 5));
        this.addComponentListener(new ComponentAdapter()
        {
            @Override
            public void componentResized(ComponentEvent e)
            {
                super.componentResized(e);
                table.setRowHeight((e.getComponent().getHeight() - 54));
            }
        });
        this.add(containerControls, BorderLayout.PAGE_START);
        this.add(scrollPane, BorderLayout.CENTER);
        containerControls.add(labelCurrentWeek);

        //initialize month label
        lblMonth = new JLabel();
        containerControls.add(lblMonth);

        //Set row/column count
        mtblCalendar.setRowCount(1);

        preButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (calc.currentWeek == 1) {
                    calc.currentWeek = 52;
                    calc.currentYear -= 1;
                } else {
                    calc.currentWeek -= 1;
                }
                DateTime weekStartDate = new DateTime().withWeekOfWeekyear(calc.currentWeek);
                calc.currentMonth = weekStartDate.monthOfYear().get() - 1;
                refreshCalendar(calc.currentWeek, calc.currentMonth, calc.currentYear);
            }
        });

        nextButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (calc.currentWeek == 52){
                    calc.currentWeek = 1;
                    calc.currentYear += 1;
                } else {
                    calc.currentWeek += 1;
                }
                DateTime weekStartDate = new DateTime().withWeekOfWeekyear(calc.currentWeek);
                calc.currentMonth = weekStartDate.monthOfYear().get() - 1;
                refreshCalendar(calc.currentWeek, calc.currentMonth, calc.currentYear);
            }
        });

//        //TODO: KALENDER WEERGEVEN VAN GESELECTEERDE MAAND VANUIT COMBOBOX (CALC.CURRENTWEEK UPDATEN ZODRA MAAND VERANDERD)
//        comboBoxMonth.addActionListener(new ActionListener()
//        {
//            @Override
//            public void actionPerformed(ActionEvent e)
//            {
//                if (comboBoxMonth.getSelectedItem() != null) {
//                    calc.currentMonth = comboBoxMonth.getSelectedIndex();
//
//                    Calendar cal = Calendar.getInstance();
//                    cal.set(Calendar.YEAR, calc.currentYear);
//                    cal.set(Calendar.MONTH, calc.currentMonth);
//                    cal.set(Calendar.DAY_OF_MONTH, calc.getRealDay());
//                    int start = cal.get(Calendar.WEEK_OF_YEAR);
//
//                    refreshCalendar(start, calc.currentMonth, calc.currentYear);
//                }
//            }
//        });


        //ActionListeners
        comboBoxYear.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (comboBoxYear.getSelectedItem() != null) {
                    String b = comboBoxYear.getSelectedItem().toString();
                    calc.currentYear = Integer.parseInt(b);
                    refreshCalendar(calc.currentWeek, calc.currentMonth, calc.currentYear);
                }
            }
        });

        //Refresh calendar
        refreshCalendar(calc.getRealWeek(), calc.getRealMonth(), calc.getRealYear());
    }

    @Override
    protected void addHeaders() {
        //Add headers
        String[] headers = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        for(int i = 0; i< 7;i++) {
            mtblCalendar.addColumn(headers[i]);
        }
    }

    void refreshCalendar(int week, int month, int year)
    {
        newMonth = month;
        newYear = year;

        //Update label en comboboxes
        labelCurrentWeek.setText("Week " + week);
        lblMonth.setText(months[month]);
        comboBoxYear.setSelectedItem(String.valueOf(year));

        preButton.setEnabled(true);
        nextButton.setEnabled(true);

        if (week == 1 && month == 0 && year <= calc.getRealYear() - 10)
        {
            preButton.setEnabled(false);
        } //Too early

        if (week == 52 && month == 11 && year >= calc.getRealYear() + 100)
        {
            nextButton.setEnabled(false);
        } //Too late

        //Clear table
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                mtblCalendar.setValueAt(null, i, j);
            }
        }

        //Draw calendar
        YearWeek yearWeek = YearWeek.of( year , week );
        for (int i = 1; i <= 7; i++)
        {
            LocalDate localDate = yearWeek.atDay(DayOfWeek.of(i)) ;
            int dayOfMonth = localDate.getDayOfMonth();
            int column = i - 1;
            mtblCalendar.setValueAt(dayOfMonth, 0, column);
        }

        //Set cell renderer
        CustomTableCellRenderer cellRenderer = new CustomTableCellRenderer();
        table.setDefaultRenderer(table.getColumnClass(0), cellRenderer);
    }
}
