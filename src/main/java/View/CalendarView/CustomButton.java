package View.CalendarView;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CustomButton extends JButton
{

    public CustomButton(String text){
        this.setText(text);
        this.setFocusable(false);
        this.setBorderPainted(false);
        this.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.decode("#E8E6E6")));
    }

    public CustomButton setButtonOption1(){
        this.setContentAreaFilled(false);
        this.setFont(new Font("Helvetica", Font.BOLD, 20));
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                setForeground(Color.decode("#20CE88"));
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                setForeground(Color.BLACK);
            }
        });
        return this;
    }

    public CustomButton setButtonOption2(){
        this.setForeground(Color.WHITE);
        this.setBackground(Color.decode("#04D449"));
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                setBackground(Color.black);
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                setBackground(Color.decode("#04D449"));
            }
        });
        return this;
    }

    public void setHighlight(){
        this.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.decode("#20CE88")));
    }
}
