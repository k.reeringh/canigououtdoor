package View.CalendarView;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class CustomTableCellRenderer extends DefaultTableCellRenderer
{
    // att

    DateCalculator calc = new DateCalculator();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        setForeground(Color.BLACK);
        setBackground(Color.decode("#E8E6E6"));

        if (value != null){
            if (Integer.parseInt(value.toString()) == calc.getRealDay() &&
                    CalendarView.newMonth == calc.getRealMonth() &&
                    CalendarView.newYear == calc.getRealYear()) {
                setBackground(new Color(220, 220, 255));
            } else {
                setBackground(Color.decode("#E8E6E6"));
            }
        }
        return c;

    }
}
