package View.CalendarView;

import Controller.CalendarController;
import View.DayCalendarView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.GregorianCalendar;

//TODO: CLEAN OP CODE
//TODO: ADD MORE STYLING
//TODO: ADD MONTH LABEL TO MONTH CALENDAR
//TODO: PUT CODE IN CONTROLLER CLASS AND DELETE MODEL CLASSES OF CALENDAR
//TODO: SPLIT CODE INTO METHODS
//TODO: ADD COMMENTS

public abstract class CalendarView extends JPanel
{


    public JLabel labelCurrentMonth;
    public JLabel labelCurrentWeek;
    public CustomButton preButton, nextButton;
    public JComboBox comboBoxYear;
    public JComboBox comboBoxMonth;
    public JLabel lblMonth;
    public JComboBox comboBoxFamilyName;
    public JTable table;
    public DefaultTableModel mtblCalendar;
    public DateCalculator calc = new DateCalculator();
    static int newMonth;
    static int newYear;
    static String[] months = {"January", "February", "March", "April", "May",
            "June", "July", "August", "September", "October", "November", "December"};
    public JScrollPane scrollPane;
    public JPanel containerControls;
    private CalendarController controller;
    public CalendarView()
    {

        //Create controls
        controller = new CalendarController(this);
        labelCurrentMonth = new JLabel("January", SwingConstants.CENTER);
        comboBoxYear = new JComboBox();
        comboBoxFamilyName = new JComboBox();
        preButton = new CustomButton("Vorige");
        nextButton = new CustomButton("Volgende");
        mtblCalendar = new DefaultTableModel()
        {
            public boolean isCellEditable(int rowIndex, int mColIndex)
            {
                return false;
            }
        };
        table = new JTable(mtblCalendar);
        scrollPane = new JScrollPane(table);
        preButton.setButtonOption2();
        nextButton.setButtonOption2();

        //Start calendar view with a view of real week or month
        calc.currentMonth = calc.getRealMonth();
        calc.currentYear = calc.getRealYear();
        calc.currentWeek = calc.getRealWeek();

        //Set container for controls
        containerControls = new JPanel(new GridLayout(0,6, 5, 5));
        containerControls.add(preButton);
        containerControls.add(nextButton);
        containerControls.add(comboBoxYear);
        containerControls.add(comboBoxFamilyName);

        labelCurrentWeek = new JLabel("Week 1", SwingConstants.CENTER);
        labelCurrentMonth = new JLabel("January", SwingConstants.CENTER);

        addHeaders();

        //Table settings
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        table.getCellSelectionEnabled();
        table.setRowSelectionAllowed(false);
        table.setBackground(Color.decode("#E8E6E6"));


        //Adding listener to the jtable so you can open Day calendar view
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount()==2) //making sure that the user has to press the mouse 2 times.
                {
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();  //getting row index
                    int column = target.getSelectedColumn(); // getting column index

                    String date = String.valueOf(table.getModel().getValueAt(row,column)); //getting the date (For example 23)
                    String year = String.valueOf(comboBoxYear.getSelectedItem()); //getting year
                    String month = (String.valueOf(labelCurrentMonth.getText()));//month
                    String day = table.getColumnName(column);// getting the day name (mon)
                    System.out.println("DAY"+day+"Date"+date+" Year "+ year + "Mounth" + month);
                    try {
                        JFrame frame = new JFrame();
                        DayCalendarView dayCalendarView = new DayCalendarView(day,date,month.substring(0,3) ,year );
                        frame.setSize(dayCalendarView.getSize());
                        frame.add(dayCalendarView);
                        frame.setVisible(true);
                    }
                    catch (IOException x)
                    {
                        x.printStackTrace();
                    }
                }
            }
        });
        //Set row/column count
        mtblCalendar.setColumnCount(7);

        //Populate comboBoxYear
        for(int i = calc.getRealYear() - 100; i <=calc.getRealYear() +100;i++) {
            comboBoxYear.addItem(String.valueOf(i));
        }
    }

    protected abstract void addHeaders();

    public void refreshCalendar(){

    }

    //Populate comboBoxFamilyName
    public void configureView(String[] familyNames) {
        for (int i = 0; i < familyNames.length; i++) {
            comboBoxFamilyName.addItem(familyNames[i]);
        }
        System.out.println(familyNames);
    }

}

