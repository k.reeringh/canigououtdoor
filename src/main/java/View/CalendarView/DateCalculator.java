package View.CalendarView;

import java.util.GregorianCalendar;

public class DateCalculator extends java.util.GregorianCalendar
{
    int realYear, realMonth, realDay, currentYear, currentMonth, realWeek, currentWeek;

    public int getRealDay(){
        realDay = this.get(java.util.GregorianCalendar.DAY_OF_MONTH);
        return realDay;
    }

    public int getRealWeek(){
        realWeek = this.get(GregorianCalendar.WEEK_OF_YEAR);
        return realWeek;
    }
    public int getRealMonth(){
        realMonth = this.get(java.util.GregorianCalendar.MONTH);
        return realMonth;
    }

    public int getRealYear(){
        realYear =this.get(java.util.GregorianCalendar.YEAR);
        return realYear;
    }

    public int getCurrentMonth(){
        currentMonth = realMonth;
        return currentMonth;
    }

    public int getCurrentYear(){
        currentYear = realYear;
        return currentYear;
    }

    public int getCurrentWeek(){
        currentWeek = realWeek;
        return currentWeek;
    }
}
