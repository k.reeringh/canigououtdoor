package View;

import Controller.LoginController;
import Controller.RegisterController;
import View.CalendarView.CustomButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

//Geeft registratie formulier voor een familie weer en stuurt gegevens op naar de RegistratieController

public class RegisterView extends JPanel {

    JPanel pnlCompHolder;
    JLabel lblTitle;

    JLabel lblSearchFam;
    JComboBox cbbFamilies;

    JLabel lblName;
    JTextField tfldName;
    JLabel lblCampNumb;
    JTextField tfldCampNumb;
    JLabel lblTel;
    JTextField tfldTel;
    JLabel lblPayed;
    JTextField tfldPayed;
    JLabel lblContacted;
    JTextField tfldContacted;
    JLabel lblBriefed;
    JTextField tfldBriefed;

    CustomButton submit;
    CustomButton reset;

    JLabel lblAge;
    JTextField tfldAge;
    JLabel lblWeight;
    JTextField tfldWeight;
    JLabel lblLength;
    JTextField tfldLength;
    JLabel lblShoeSize;
    JTextField tfldShoeSize;

    CustomButton add;
    CustomButton remove;

    String fontFamily = "Helvetica";
    int textSize = 16;

    JPanel familyMembersPanel;
    JPanel familyMemberControlPanel;
    JPanel familyMemberPanelHolder;

    int i = 0;

    JPanel subPanel;

    Component[] components;

    boolean lessThanFour = true;

    //TODO: GIVE USER OPPORTUNITY TO SEARCH FOR FAMILY AND DISPLAY CHOSEN FAMILY DATA INTO TEXTFIELDS

    RegisterView(){

        //setup main panel
        this.setLayout(new BorderLayout());

        makeRegisterFamilyFormGUI();
        makeRegisterFamilyMemberFormGUI();

        //add panels to main panel
        this.add(pnlCompHolder, BorderLayout.WEST);
        this.add(familyMembersPanel, BorderLayout.CENTER);
    }

    public void makeRegisterFamilyFormGUI(){
        RegisterController registerController=new RegisterController();
        pnlCompHolder = new JPanel();
        pnlCompHolder.setLayout(new GridBagLayout());

        //initalize components
        lblSearchFam = new JLabel("Zoek familie");
        cbbFamilies = new JComboBox();

        lblTitle = new JLabel("Registreer familie");
        lblName = new JLabel("Familie naam: ");
        tfldName = new JTextField();
        lblCampNumb = new JLabel("Camping nummer: ");
        tfldCampNumb = new JTextField();
        lblTel = new JLabel("Telefoon nummer: ");
        tfldTel = new JTextField();
        lblPayed = new JLabel("Betaald: ");
        tfldPayed = new JTextField();
        lblContacted = new JLabel("Geapt: ");
        tfldContacted = new JTextField();
        lblBriefed = new JLabel("Gebriefd: ");
        tfldBriefed = new JTextField();

        submit = new CustomButton("Submit");
        reset = new CustomButton("Reset");

//    submit.addActionListener(new ActionListener(){
//    @Override
//    public void actionPerformed(ActionEvent e) {
//
//});

        //setup contraints for panel holder
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(20,20,20,20);

        //add components to panel holder
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        lblTitle.setFont(new Font(fontFamily, Font.BOLD, 32));
        pnlCompHolder.add(lblTitle, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;
        lblSearchFam.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblSearchFam, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 1;
        pnlCompHolder.add(cbbFamilies, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        lblName.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblName, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 2;
        tfldName.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldName, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        lblCampNumb.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblCampNumb, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 3;
        tfldCampNumb.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldCampNumb, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 4;
        lblTel.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblTel, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 4;
        tfldTel.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldTel, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 5;
        lblPayed.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblPayed, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 5;
        tfldPayed.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldPayed, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 6;
        lblContacted.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblContacted, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 6;
        tfldContacted.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldContacted, c);

        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 7;
        lblBriefed.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(lblBriefed, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 7;
        tfldBriefed.setFont(new Font(fontFamily, Font.BOLD, textSize));
        pnlCompHolder.add(tfldBriefed, c);

        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 8;
        submit.setFont(new Font(fontFamily, Font.BOLD, textSize));
        submit.setButtonOption2();
        pnlCompHolder.add(submit, c);

        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 8;
        reset.setFont(new Font(fontFamily, Font.BOLD, textSize));
        reset.setButtonOption2();
        pnlCompHolder.add(reset, c);
    }

    public void makeRegisterFamilyMemberFormGUI() {
        familyMembersPanel = new JPanel(new BorderLayout());

        familyMemberControlPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 100, 10));

        add = new CustomButton("Voeg familielid toe");
        add.setFont(new Font(fontFamily, Font.BOLD, textSize));
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                i++;

                if (lessThanFour){
                    if(i <= 4){
                        subPanel = makeFamilyMemberPanel(new JPanel());

                        familyMemberPanelHolder.add(subPanel);
                        familyMemberPanelHolder.revalidate();
                        familyMemberPanelHolder.repaint();
                    } else {
                        lessThanFour = false;
                    }
                }
            }
        });
        add.setButtonOption2();
        familyMemberControlPanel.add(add);

        remove = new CustomButton("Verwijder familielid");
        remove.setFont(new Font(fontFamily, Font.BOLD, textSize));
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (!lessThanFour){
                    i = 3;
                    lessThanFour = true;

                    removeFamilyMemberPanel();
                } else {
                    i--;

                    removeFamilyMemberPanel();
                }
            }
        });
        remove.setButtonOption2();
        familyMemberControlPanel.add(remove);

        familyMemberPanelHolder = new JPanel();
        familyMemberPanelHolder.setLayout(new FlowLayout(FlowLayout.CENTER));
        familyMemberPanelHolder.setBackground(Color.decode("#E8E6E6"));

        familyMembersPanel.add(familyMemberControlPanel, BorderLayout.PAGE_START);
        familyMembersPanel.add(familyMemberPanelHolder, BorderLayout.CENTER);
    }

    public JPanel makeFamilyMemberPanel(JPanel panel){
        panel.setPreferredSize(new Dimension(400, 150));
        panel.setBackground(Color.decode("#04D449"));
        panel.setLayout(new GridBagLayout());

        lblAge = new JLabel("Leeftijd: ");
        tfldAge = new JTextField();
        lblWeight = new JLabel("Gewicht: ");
        tfldWeight = new JTextField();
        lblLength = new JLabel("Lengte: ");
        tfldLength = new JTextField();
        lblShoeSize = new JLabel("Schoenmaat: ");
        tfldShoeSize = new JTextField();

        //setup contraints for panel holder
        GridBagConstraints d = new GridBagConstraints();
        d.fill = GridBagConstraints.HORIZONTAL;
        d.insets = new Insets(3,3,3,3);

        d.gridx = 0;
        d.gridy = 0;
        panel.add(lblAge, d);

        d.gridx = 1;
        d.gridy = 0;
        tfldAge.setPreferredSize(new Dimension(150, 20));
        panel.add(tfldAge, d);

        d.gridx = 0;
        d.gridy = 1;
        panel.add(lblWeight, d);

        d.gridx = 1;
        d.gridy = 1;
        tfldWeight.setPreferredSize(new Dimension(150, 20));
        panel.add(tfldWeight, d);

        d.gridx = 0;
        d.gridy = 2;
        panel.add(lblLength, d);

        d.gridx = 1;
        d.gridy = 2;
        tfldLength.setPreferredSize(new Dimension(150, 20));
        panel.add(tfldLength, d);

        d.gridx = 0;
        d.gridy = 3;
        panel.add(lblShoeSize, d);

        d.gridx = 1;
        d.gridy = 3;
        tfldShoeSize.setPreferredSize(new Dimension(150, 20));
        panel.add(tfldShoeSize, d);

        return panel;
    }

    public void removeFamilyMemberPanel(){
        components = familyMemberPanelHolder.getComponents();
        ArrayList<Component> components1 = new ArrayList<>();
        components1.addAll(Arrays.asList(components));

        int index = components1.size() -1;

        try {
            familyMemberPanelHolder.remove(index);
            familyMemberPanelHolder.revalidate();
            familyMemberPanelHolder.repaint();
        } catch (ArrayIndexOutOfBoundsException e) {
            i = 0;
            e.printStackTrace();
        }
    }
}