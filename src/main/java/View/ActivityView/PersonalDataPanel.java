package View.ActivityView;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

public class PersonalDataPanel extends JPanel {
    //private JPanel panel;
    private JTable mainTable;
    private JScrollPane jScrollPane;
    private Object rows[][] = {{"test","74"},{"T@#@#SDADASDASD@!#!@@@@@@@@@@@@@@EDASSSSSSSSSSSSSSSSSSSA"},};
    private String columns[] = {"Naam","Gewicht", "Pakmaat gewicht", "Lengte Pakmaat"," lengte def pak maat","Schoenmaat"};
    private TableModel model;

    public PersonalDataPanel() {
        model =new DefaultTableModel(rows, columns);
        mainTable = new JTable(model)
        {
            public boolean isCellEditable(int rowIndex, int mColIndex)
            {
                return false;
            }};
        mainTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jScrollPane =new JScrollPane(mainTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//        panel = new JPanel();
        this.setLayout(new BorderLayout());
        this.add(jScrollPane, BorderLayout.CENTER);
        for(int x =0 ; x<mainTable.getColumnCount();x++)
        {
            packColumn(mainTable,x);
        }
    }

    public static void packColumn(JTable table, int vColIndex) {
        DefaultTableColumnModel colModel = (DefaultTableColumnModel)table.getColumnModel();
        TableColumn col = colModel.getColumn(vColIndex);
        int width = 0;

        // Get width of column header
        TableCellRenderer renderer = col.getHeaderRenderer();
        if (renderer == null) {
            renderer = table.getTableHeader().getDefaultRenderer();
        }
        java.awt.Component comp = renderer.getTableCellRendererComponent(
                table, col.getHeaderValue(), false, false, 0, 0);
        width = comp.getPreferredSize().width;

        // Get maximum width of column data
        for (int r=0; r<table.getRowCount(); r++) {
            renderer = table.getCellRenderer(r, vColIndex);
            comp = renderer.getTableCellRendererComponent(
                    table, table.getValueAt(r, vColIndex), false, false, r, vColIndex);
            width = Math.max(width, comp.getPreferredSize().width);
        }

//        // Add margin
//        width += 2*margin;

        // Set the width
        col.setPreferredWidth((int)(width*1.3));

    }

}

