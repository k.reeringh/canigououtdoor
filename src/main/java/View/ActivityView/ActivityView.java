package View.ActivityView;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ActivityView extends JPanel{
    //JPanel mainPanel;
    private CustomButtonActivityView status;
    private CustomButtonActivityView personalData;
    private JLabel activityName;
    private StatusPanel statusTable;
    private PersonalDataPanel personalDataTable;
    private CustomButtonActivityView addButton;

    public ActivityView() throws IOException  {
        this.setLayout(null);
        System.out.println("Width = "+getWidthOfScreen());
        System.out.println("Height = " + getHeightOfScreen());
        this.setSize((int)(getWidthOfScreen()/(1.7)),(int)(getHeightOfScreen()/(1.7)));
        this.setVisible(true);
        this.add(setJLabelActivityName());
        this.add(setStatusButton());
        this.add(setPersonalDataButton());
        this.add(setStatusTable());
        this.add(setPersonalDataTable());
        this.add(setAddButton());
    }

    private JLabel setJLabelActivityName()
    {
        activityName = new JLabel();
        activityName.setText("ActivityName");
        activityName.setFont(new Font("Dialog",Font.PLAIN,30));
        activityName.setBounds(1,0,(int)(getWidthOfScreen()/3.84),(int)(getHeightOfScreen()/18.28));

        return activityName;
    }

    private CustomButtonActivityView setAddButton() throws IOException {
        BufferedImage buttonIcon = ImageIO.read(new File("add.png"));
        addButton = new CustomButtonActivityView(new ImageIcon(buttonIcon));
        addButton.setButton();
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        addButton.setBounds((int)(getWidthOfScreen()/2.2),(int)(getHeightOfScreen()/108),(int)(getWidthOfScreen()/15.36),(int)(getHeightOfScreen()/8.64));

        return addButton;
    }

    private CustomButtonActivityView setStatusButton()
    {
        status = new CustomButtonActivityView("Status");
        status.setButton();
        status.setBounds(1,(int)(getHeightOfScreen()/20),(int)(getWidthOfScreen()/15.36),(int)(getHeightOfScreen()/21.6));
        status.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ActivityView.this.getComponent(4).setVisible(false);
                ActivityView.this.getComponent(3).setVisible(true);
            }
        });

        return status;
    }
    private CustomButtonActivityView setPersonalDataButton()
    {
        personalData = new CustomButtonActivityView("Persoonsgegevens");
        personalData.setButton();
        personalData.setBounds ((int)(getWidthOfScreen()/16),(int)(getHeightOfScreen()/20),(int)(getWidthOfScreen()/8),(int)(getHeightOfScreen()/21.6));
        personalData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ActivityView.this.getComponent(4).setVisible(true);
                ActivityView.this.getComponent(3).setVisible(false);
            }
        });
        return personalData;
    }



    private JPanel setStatusTable()
    {
        statusTable = new StatusPanel();
        JPanel table;
        table = statusTable;
        table.setBounds(1,(int)(getHeightOfScreen()/9.5),(int)(getWidthOfScreen()/2),(int)(getHeightOfScreen()/2.4));
        return table;
    }

    private JPanel setPersonalDataTable()
    {
        personalDataTable = new PersonalDataPanel();
        JPanel table;
        table = personalDataTable;
        table.setBounds(1,(int)(getHeightOfScreen()/9.5),(int)(getWidthOfScreen()/2),(int)(getHeightOfScreen()/2.4));
        return table;
    }

    private int getHeightOfScreen()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)screenSize.getHeight();
        return height;
    }

    private int getWidthOfScreen()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int)screenSize.getWidth();
        return width;
    }
}

