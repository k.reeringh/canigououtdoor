package View.ActivityView;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;

public class StatusPanel extends JPanel{
   // JPanel mainPanel;
    private JTable table;
    private JScrollPane jScrollPane;
    private Object rows[][] = {{"test"}};
    private String columns[] ={"Naam","Leeftijd","Camping nu","Ingeschreven","Betaald","Geapt","Gebriefd"};
    private TableModel model;

    public StatusPanel()
    {
      //  mainPanel = new JPanel();
        model = new DefaultTableModel(rows,columns);
        table = new JTable(model)
        {
            public boolean isCellEditable(int rowIndex, int mColIndex)
        {
            return false;
        }};

        jScrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.setLayout(new BorderLayout());
        this.add(jScrollPane,BorderLayout.CENTER);
        this.setVisible(true);
        for(int x =0 ; x<table.getColumnCount();x++)
        {
            PersonalDataPanel.packColumn(table,x);
        }
    }
}
