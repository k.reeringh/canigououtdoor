package View.ActivityView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CustomButtonActivityView extends JButton {

    public CustomButtonActivityView(String text) {
        this.setText(text);
        this.setFocusable(false);
        this.setBorderPainted(false);
    }
    public CustomButtonActivityView(ImageIcon image)
    {
        this.setIcon(image);
        this.setFocusable(false);
        this.setBorder(BorderFactory.createEmptyBorder());
        this.setBorderPainted(false);
        this.setContentAreaFilled(false);

    }

    public CustomButtonActivityView setButton()
    {
        this.setForeground(Color.WHITE);
        this.setBackground(Color.decode("#04D449"));
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                setBackground(Color.black);
            }

            @Override
            public void mouseExited(MouseEvent e)
            {
                setBackground(Color.decode("#04D449"));
            }
        });
        return this;
    }

}