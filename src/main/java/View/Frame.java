package View;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame
{
    static Container pane;
    static CardLayout cards;

    public Frame(){
        //Prepare frame
        setSize(1100, 760);
        setLocationRelativeTo(null);

        cards = new CardLayout();

        pane = this.getContentPane();
        pane.setBackground(Color.WHITE);
        pane.setLayout(cards);

        pane.add(new LoginView());
        pane.add(new MainView(), "main");

        //Settings frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(pane);
        setResizable(true);
        setVisible(true);
    }
}
