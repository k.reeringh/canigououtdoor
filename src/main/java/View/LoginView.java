package View;
import Controller.LoginController;
import View.Frame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;


public class LoginView extends JPanel {

    private JLabel userLabel;
    private JTextField userText;
    private JLabel passwordLabel;
    private JPasswordField passwordText;
    private JButton loginButton;
    private JButton resetButton;
    private JLabel message;
    private LoginController controller;

    public LoginView(){ //exception thrown for file
        controller= new LoginController(this);
        userLabel = new JLabel("Gebruiker");
        passwordLabel = new JLabel("Wachtwoord");
        loginButton = new JButton("Log In");
        resetButton = new JButton("Reset");
        userText = new JTextField(5); //Determine length of textfield
        passwordText = new JPasswordField(5);// Difference between this and the textfield is the fact that it hides user input
        message = new JLabel("");

        //LoginView panel
        this.setLayout(new GridBagLayout()); // set to null because bounds are set manually
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(3,3,3,3);

        // Add image to frame
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel(new ImageIcon("background3.jpg")), c);

        // Add login controller panel to main panel
        c.gridx = 0;
        c.gridy = 1;
        JPanel panel = new JPanel();

        panel.setLayout(new GridBagLayout());
        this.add(panel, c);

        // setting positions
        c.gridx = 0;
        c.gridy = 0;
        panel.add(userLabel, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(passwordLabel, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 0;
        panel.add(userText, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 1;
        panel.add(passwordText, c);

        c.insets = new Insets(3,3,20,3);
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 2;
        panel.add(loginButton, c);

        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 2;
        panel.add(resetButton, c);

        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 3;
        panel.add(message, c);

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == resetButton) {
                   userText.setText("");
                   passwordText.setText("");
                   message.setText("");
                }
            }
        });

// Controller checks if credentials are valid
        loginButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean isValid = controller.checkCredentials(userText.getText(), new String(passwordText.getPassword()));
                if (isValid) {
                    message.setForeground(Color.green);
                    message.setText("Inloggen succesvol!");
                    CardLayout cardLayout = (CardLayout) Frame.pane.getLayout();
                    cardLayout.next(Frame.pane);
                }
                message.setForeground(Color.red);
                message.setText("Inloggen mislukt!");
            }
        });
    }
}

