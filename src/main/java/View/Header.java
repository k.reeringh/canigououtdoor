package View;

import View.CalendarView.CustomButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Header extends JPanel
{
    public static CustomButton btn1 = new CustomButton("Registreer familie");
    public static CustomButton btn2 = new CustomButton("Maand kalender");
    public static CustomButton btn3 = new CustomButton("logout");
    public static CustomButton btn4 = new CustomButton("Week kalender");

    public Header(){
        setPreferredSize(new Dimension(0, 75));
        setBackground(Color.decode("#E8E6E6"));
        setLayout(new FlowLayout(FlowLayout.TRAILING, 20, 23));

        add(btn1);
        add(btn2);
        add(btn3);
        add(btn4);

        btn1.setButtonOption1();
        btn2.setButtonOption1();
        btn3.setButtonOption1();
        btn4.setButtonOption1();

        btn1.setVerticalAlignment(CustomButton.CENTER);
        btn2.setVerticalAlignment(CustomButton.CENTER);
        btn3.setVerticalAlignment(CustomButton.CENTER);
        btn4.setVerticalAlignment(CustomButton.CENTER);
    }
}