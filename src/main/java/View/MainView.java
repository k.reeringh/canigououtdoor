package View;

import View.CalendarView.CalendarView;
import View.CalendarView.CustomButton;
import View.CalendarView.MonthCalendarView;
import View.CalendarView.WeekCalendarView;
import javax.swing.*;
import java.awt.*;
    //Geeft header weer en mogelijkheid om vanaf hier naar RegistratieView, BookingView en CalendarView te gaan
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainView extends JPanel {


    public CardLayout cl;
    Header header;
    JPanel body;

   public MainView() {
       //Prepare panel
       this.setBounds(0,0,1100, 700);
       this.setBackground(Color.red);
       this.setLayout(new BorderLayout());

       header = new Header();
       cl = new CardLayout();
       body = new JPanel();
       body.setLayout(cl);

       this.add(header, BorderLayout.PAGE_START);
       this.add(body, BorderLayout.CENTER);

       body.add(new MonthCalendarView(), "month_calendar");
       body.add(new RegisterView(), "register");
       body.add(new WeekCalendarView(), "week_calendar");

       Header.btn1.addActionListener(new ActionListener()
       {
           @Override
           public void actionPerformed(ActionEvent e)
           {
               cl.show(body, "register");

               generateBorderOnCurrent(e);
           }
       });
       Header.btn2.addActionListener(new ActionListener()
       {
           @Override
           public void actionPerformed(ActionEvent e)
           {
               cl.show(body, "month_calendar");

               generateBorderOnCurrent(e);
           }
       });
       Header.btn3.addActionListener(new ActionListener()
       {
           @Override
           public void actionPerformed(ActionEvent e)
           {
               CardLayout cardLayout = (CardLayout) Frame.pane.getLayout();
               cardLayout.previous(Frame.pane);

               generateBorderOnCurrent(e);
           }
       });
       Header.btn4.addActionListener(new ActionListener()
       {
           @Override
           public void actionPerformed(ActionEvent e)
           {
               cl.show(body, "week_calendar");

               generateBorderOnCurrent(e);
           }
       });
   }

    public void generateBorderOnCurrent(ActionEvent e){
        Header.btn1.setBorderPainted(false);
        Header.btn2.setBorderPainted(false);
        Header.btn3.setBorderPainted(false);
        Header.btn4.setBorderPainted(false);

        CustomButton src = (CustomButton) e.getSource();
        src.setBorderPainted(true);
        src.setHighlight();
    }
}
