package View;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import View.ActivityView.*;

public class DayCalendarView extends JPanel{
    private JPanel activityPanel;
    private JLabel day;
    private JLabel date;
    private JLabel year;
    private ActivityView act = new ActivityView();
    private ActivityView activityView = new ActivityView();
    private JScrollPane jScrollPane;

    public DayCalendarView(String day,String date , String month , String year) throws IOException {
        // dayPanel = new JPanel();
        this.setLayout(null);
        this.setSize((int)(getWidthOfScreen()/1.3),(int) (getHeightOfScreen()/1.3));
        this.add(setDayLabel(day));
        this.add(setDateLabel(date,month ));
        this.add(setYearLabel(year));
        this.add(setActivityPanel());

    }


    private JLabel setDayLabel(String day1)
    {
        day = new JLabel();
        day.setText(day1);
        day.setFont(new Font("Dialog",Font.PLAIN,40));
        day.setBounds(1,0,350,50);
        return day;
    }

    private JLabel setDateLabel(String date1 , String month)
    {
        date = new JLabel();
        date.setText(date1 + " "+ month);
        date.setFont(new Font("Dialog",Font.PLAIN,40));
        date.setBounds(1,75,400,50);

        return date;
    }

    private JLabel setYearLabel(String year1)
    {
        year = new JLabel();
        year.setText(year1);
        year.setFont(new Font("Dialog",Font.PLAIN,40));
        year.setBounds(1,150,400,50);

        return year;
    }

    private JPanel setActivityPanel()
    {
        JPanel mainPanel = new JPanel(new BorderLayout());

        activityPanel = new JPanel(new GridLayout(2,1,100,0));
        activityPanel.setSize((int)(getWidthOfScreen()/1.7),(int)(getHeightOfScreen()/1.7));
        activityPanel.setPreferredSize(new Dimension(activityPanel.getWidth(),act.getHeight()*2));
        activityPanel.setBackground(Color.BLUE);
        activityPanel.add(act);
        activityPanel.setMinimumSize(act.getSize());
        activityPanel.add(activityView);
        jScrollPane = new JScrollPane(activityPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jScrollPane.setVisible(true);
        mainPanel.setSize((int)(getWidthOfScreen()/1.6),(int)(getHeightOfScreen()/1.6));
        mainPanel.setBounds(150,10,mainPanel.getWidth(),mainPanel.getHeight());
        mainPanel.setBackground(Color.yellow);
        mainPanel.add(jScrollPane,BorderLayout.CENTER);

        return mainPanel;
    }
    private int getHeightOfScreen()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)screenSize.getHeight();
        return height;
    }

    private int getWidthOfScreen()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int)screenSize.getWidth();
        return width;
    }
}
